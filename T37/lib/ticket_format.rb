class TicketFormat
  VALUE = { 'id'            => '工单编号',
            'type'          => '工单类型',
            'source'        => '工单来源',
            'customer'      => '顾客信息',
            'category'      => '工单类别',
            'symptom'       => '表现症状',
            'descr'         => '详细描述',
            'state'         => '状态',
            'location'      => '地理位置',
            'team'          => '负责处理的团队',
            'solution'      => '解决方法描述',
            'case'          => '故障缘由',
            'matter'        => '问题说明',
            'satisfaction'  => '用户满意度',
            'feedback'      => '用户反馈',
            'accepted_at'   => '受理时间',
            'dispatched_at' => '派单时间',
            'closed_at'     => '关闭时间'
            }

  def self.translate columns
    columns.map do | column |
      VALUE.key column
    end
  end
end
