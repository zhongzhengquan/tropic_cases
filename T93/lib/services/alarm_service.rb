class AlarmService
  include HTTParty
  digest_auth ENV['IMCRS_USER'], ENV['IMCRS_PASSWD']
  base_uri ENV['IMCRS_BASE_URI']

  def query(options = {})
    response = self.class.get("/fault/alarm", headers: {'Accept' => 'application/json'}, query: options)
    if options[:total] || options['total']
      response.headers['total'] ? response.headers['total'] : 0
    elsif response['alarm'].nil?
      []
    elsif !response['alarm'].instance_of?(Array)
      [response['alarm']]
    else
      response['alarm']
    end
  end
end
